//
//  GameInstructions.m
//  StarCommander
//
//  Created by Michael on 7/26/15.
//  Copyright (c) 2015 Michael Usry. All rights reserved.
//

#import "GameInstructions.h"
#import <CoreMotion/CoreMotion.h>
#import "MainMenu.h"
#import <AVFoundation/AVFoundation.h>
#import "GameScene.h"
#import "GameCredits.h"


@implementation GameInstructions
{
    SKLabelNode *gameInstructions, *gameInstructions1, *gameInstructions2, *gameInstructions3;
    SKLabelNode *gameInstructions4, *gameInstructions5, *gameInstructions6, *gameInstructions7;
    SKLabelNode *gameInstructions8, *gameInstructions9, *gameInstructions10, *gameInstructions11;
    NSString *gameInstructionsText,*gameInstructionsText1,*gameInstructionsText2,*gameInstructionsText3;
    NSString *gameInstructionsText4,*gameInstructionsText5,*gameInstructionsText6,*gameInstructionsText7;
    NSString *gameInstructionsText8,*gameInstructionsText9,*gameInstructionsText10,*gameInstructionsText11;
    SKLabelNode *gameCreditsLabel;
    SKLabelNode *mainMenuLabel;
    SKLabelNode *beginGameLabel;

}


- (void)nextScene {
    NSString *nextSceneButton;
    nextSceneButton = @"GameInstructions";
    
    SKLabelNode *myLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    myLabel.text = nextSceneButton;
    myLabel.fontSize = 30;
    myLabel.fontColor = [SKColor blackColor];
    myLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    myLabel.name = @"scene button";
    
    [self addChild:myLabel];
}

-(void)didMoveToView:(SKView *)view{
//    self.backgroundColor = [SKColor purpleColor];
    [self background];
    
    gameInstructionsText = @"Avoid or destroy the Asteroids and the enemy";
    gameInstructionsText1 = @"ships. Don't get hit by the them or the";
    gameInstructionsText2 = @"lasers.  Press the fire button to shoot.";
    gameInstructionsText3 = @"The pause button to pause the game or the";
    gameInstructionsText4 = @"shield button to protect yourself.";






    
    
/*
    Your will be flying through Asteroids and XXX Star fighters coming after you.  Either dodge them or shoot them into a million pieces.  You have two lives and shields to protect you.  Shoot the enemy by pressing the fire button (image) and turn on the shields by pressing the shield button (image).  If you get stuck pause.  When the scene ends you will go to the next one.
*/
    
//    [self nextScene];
    [self gameInstructions];
    [self MainMenuLabel];
    [self GameCreditsLabel];
    [self BeginGameLabel];

}

- (void)MainMenuLabel {
    NSString *mainMenuButton;
    mainMenuButton = @"Main Menu";
    
    mainMenuLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    mainMenuLabel.text = mainMenuButton;
    mainMenuLabel.fontSize = 30;
    mainMenuLabel.fontColor = [SKColor blackColor];
    mainMenuLabel.position = CGPointMake(CGRectGetMidX(self.frame), -100);
    mainMenuLabel.name = @"mainMenu";
    SKAction *sceneLabels = [SKAction moveToY:(self.frame.size.height/2) duration:15];
    [mainMenuLabel runAction:sceneLabels];
    
    
    [self addChild:mainMenuLabel];
}

- (void)GameCreditsLabel {
    NSString *gameCreditsButton;
    gameCreditsButton = @"Game Credits";
    
    gameCreditsLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    gameCreditsLabel.text = gameCreditsButton;
    gameCreditsLabel.fontSize = 30;
    gameCreditsLabel.fontColor = [SKColor blackColor];
    gameCreditsLabel.position = CGPointMake(CGRectGetMidX(self.frame), -170);
    gameCreditsLabel.name = @"gameCredits";
    
    SKAction *sceneLabels = [SKAction moveToY:(self.frame.size.height/2)-30 duration:15];
    [gameCreditsLabel runAction:sceneLabels];
    
    
    [self addChild:gameCreditsLabel];
}

- (void)BeginGameLabel {
    NSString *mainMenuSceneButton;
    mainMenuSceneButton = @"Begin Game";
    
    beginGameLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    beginGameLabel.text = mainMenuSceneButton;
    beginGameLabel.fontSize = 30;
    beginGameLabel.fontColor = [SKColor blackColor];
    beginGameLabel.position = CGPointMake(CGRectGetMidX(self.frame), -240);
    beginGameLabel.name = @"startGame";
    SKAction *sceneLabels = [SKAction moveToY:(self.frame.size.height/2)-60 duration:15];
    [beginGameLabel runAction:sceneLabels];
    
    [self addChild:beginGameLabel];
}

- (void)gameInstructions {
    gameInstructions = [SKLabelNode labelNodeWithFontNamed:@"Courier Bold"];
    gameInstructions.name = @"gameInstructions";
    gameInstructions.text = gameInstructionsText;
    gameInstructions.position = CGPointMake(self.frame.size.width/2, -30);
    gameInstructions.fontColor = [SKColor whiteColor];
    gameInstructions.fontSize = 16;
    
    gameInstructions1 = [SKLabelNode labelNodeWithFontNamed:@"Courier Bold"];
    gameInstructions1.name = @"gameInstructions1";
    gameInstructions1.text = gameInstructionsText1;
    gameInstructions1.position = CGPointMake(self.frame.size.width/2, -70);
    gameInstructions1.fontColor = [SKColor whiteColor];
    gameInstructions1.fontSize = 16;
    
    gameInstructions2 = [SKLabelNode labelNodeWithFontNamed:@"Courier Bold"];
    gameInstructions2.name = @"gameInstructions2";
    gameInstructions2.text = gameInstructionsText2;
    gameInstructions2.position = CGPointMake(self.frame.size.width/2, -110);
    gameInstructions2.fontColor = [SKColor whiteColor];
    gameInstructions2.fontSize = 16;
    
    gameInstructions3 = [SKLabelNode labelNodeWithFontNamed:@"Courier Bold"];
    gameInstructions3.name = @"gameInstructions2";
    gameInstructions3.text = gameInstructionsText3;
    gameInstructions3.position = CGPointMake(self.frame.size.width/2, -160);
    gameInstructions3.fontColor = [SKColor whiteColor];
    gameInstructions3.fontSize = 16;

    gameInstructions4 = [SKLabelNode labelNodeWithFontNamed:@"Courier Bold"];
    gameInstructions4.name = @"gameInstructions2";
    gameInstructions4.text = gameInstructionsText4;
    gameInstructions4.position = CGPointMake(self.frame.size.width/2, -200);
    gameInstructions4.fontColor = [SKColor whiteColor];
    gameInstructions4.fontSize = 16;
    
    
    
    SKAction *moveInstructions = [SKAction moveToY:2000 duration:40];
    [gameInstructions runAction:moveInstructions];
    [gameInstructions1 runAction:moveInstructions];
    [gameInstructions2 runAction:moveInstructions];
    [gameInstructions3 runAction:moveInstructions];
    [gameInstructions4 runAction:moveInstructions];

    
    
    [self addChild:gameInstructions];
    [self addChild:gameInstructions1];
    [self addChild:gameInstructions2];
    [self addChild:gameInstructions3];
    [self addChild:gameInstructions4];

    
}

- (void)background {
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"starBackground"];
    background.size = self.frame.size;
    background.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    [self addChild:background];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if ([node.name isEqualToString:@"startGame"]) {
        
        SKTransition *reveal = [SKTransition fadeWithDuration:3];
        
            //        GameScene *gameScene = [GameScene sceneWithSize:self.size];
        GameScene *gameScene = [GameScene sceneWithSize:self.size];
        gameScene.scaleMode = SKSceneScaleModeAspectFill;
        
        
        [self.view presentScene:gameScene transition:reveal];
    }
    
    if ([node.name isEqualToString:@"mainMenu"]) {
        
        SKTransition *reveal = [SKTransition fadeWithDuration:3];
        
        MainMenu *gameScene = [MainMenu sceneWithSize:self.size];
        gameScene.scaleMode = SKSceneScaleModeAspectFill;
        
        [self.view presentScene:gameScene transition:reveal];
    }
    
    if ([node.name isEqualToString:@"gameCredits"]) {
        
        SKTransition *reveal = [SKTransition fadeWithDuration:3];
        
        GameCredits *gameScene = [GameCredits sceneWithSize:self.size];
        gameScene.scaleMode = SKSceneScaleModeAspectFill;
        
        [self.view presentScene:gameScene transition:reveal];
    }
    
    
}



@end
