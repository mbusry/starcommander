//
//  AppDelegate.h
//  StarCommander
//
//  Created by Michael on 7/6/15.
//  Copyright (c) 2015 Michael Usry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;



@end

