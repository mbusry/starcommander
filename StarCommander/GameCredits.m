//
//  GameCredits.m
//  StarCommander
//
//  Created by Michael on 7/26/15.
//  Copyright (c) 2015 Michael Usry. All rights reserved.
//

#import <CoreMotion/CoreMotion.h>
#import "GameCredits.h"
#import <AVFoundation/AVFoundation.h>
#import "MainMenu.h"
#import "GameInstructions.h"
#import "GameScene.h"

@implementation SKScene (Unarchive)

+ (instancetype)unarchiveFromFile:(NSString *)file {
    /* Retrieve scene file path from the application bundle */
    NSString *nodePath = [[NSBundle mainBundle] pathForResource:file ofType:@"sks"];
    /* Unarchive the file to an SKScene object */
    NSData *data = [NSData dataWithContentsOfFile:nodePath
                                          options:NSDataReadingMappedIfSafe
                                            error:nil];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:self forClassName:@"SKScene"];
    SKScene *scene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [arch finishDecoding];
    
    return scene;
}

@end

@implementation GameCredits
{

    SKLabelNode *gameCreditsLabel,*gameCreditsLabel1,*gameCreditsLabel2;
    NSString *gameCreditsText,*gameCreditsText1,*gameCreditsText2,*gameCreditsText3;
    NSString *gameCreditsText4,*gameCreditsText5,*gameCreditsText6,*gameCreditsText7;
    NSString *gameCreditsText8,*gameCreditsText9,*gameCreditsText10,*gameCreditsText11;
    SKLabelNode *gameInstructionLabel;
    SKLabelNode *mainMenuLabel;
    SKLabelNode *beginGameLabel;
    
    
}

- (void)gameCredits {
    gameCreditsLabel = [SKLabelNode labelNodeWithFontNamed:@"Courier Bold"];
    gameCreditsLabel.name = @"gameCredits";
    gameCreditsLabel.text = gameCreditsText;
    gameCreditsLabel.position = CGPointMake(self.frame.size.width/2, -30);
    gameCreditsLabel.fontColor = [SKColor whiteColor];
    gameCreditsLabel.fontSize = 16;
    
    gameCreditsLabel1 = [SKLabelNode labelNodeWithFontNamed:@"Courier Bold"];
    gameCreditsLabel1.name = @"gameCredits1";
    gameCreditsLabel1.text = gameCreditsText1;
    gameCreditsLabel1.position = CGPointMake(self.frame.size.width/2, -60);
    gameCreditsLabel1.fontColor = [SKColor whiteColor];
    gameCreditsLabel1.fontSize = 16;
    
    gameCreditsLabel2 = [SKLabelNode labelNodeWithFontNamed:@"Courier Bold"];
    gameCreditsLabel2.name = @"gameCredits2";
    gameCreditsLabel2.text = gameCreditsText2;
    gameCreditsLabel2.position = CGPointMake(self.frame.size.width/2, -90);
    gameCreditsLabel2.fontColor = [SKColor whiteColor];
    gameCreditsLabel2.fontSize = 16;


    
    SKAction *moveCredits = [SKAction moveToY:2000 duration:20];
    [gameCreditsLabel runAction:moveCredits];
    [gameCreditsLabel1 runAction:moveCredits];
    [gameCreditsLabel2 runAction:moveCredits];
    
    
    [self addChild:gameCreditsLabel];
//    [gameCreditsLabel addChild:gameCreditsLabel1];
//    [gameCreditsLabel1 addChild:gameCreditsLabel2];
    [self addChild:gameCreditsLabel1];
    [self addChild:gameCreditsLabel2];


}

- (void)nextScene {
    NSString *nextSceneButton;
    nextSceneButton = @"GameCredits";
    
    SKLabelNode *myLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    myLabel.text = nextSceneButton;
    myLabel.fontSize = 30;
    myLabel.fontColor = [SKColor blackColor];
    myLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    myLabel.name = @"scene button";
    
    [self addChild:myLabel];
}

-(void)didMoveToView:(SKView *)view{
//    self.backgroundColor = [SKColor whiteColor];
    gameCreditsText  = @"Game designed . . Michael Usry";
    gameCreditsText1 = @"Game Sounds . . . sweetsoundeffects.com";
    gameCreditsText2 = @"Game Graphics . . www.kenney.nl";
    [self background];
    
//    [self nextScene];
    SKAction *sceneLabels = [SKAction moveToY:(self.frame.size.height/2) duration:30];

    [mainMenuLabel runAction:sceneLabels];
    [gameInstructionLabel runAction:sceneLabels];
    [beginGameLabel runAction:sceneLabels];
    
    [self gameCredits];
    [self GameInstructionsLabel];
    [self MainMenuLabel];
    [self BeginGameLabel];


}

- (void)background {
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"starBackground"];
    background.size = self.frame.size;
    background.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    [self addChild:background];
}

- (void)MainMenuLabel {
    NSString *mainMenuButton;
    mainMenuButton = @"Main Menu";
    
    mainMenuLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    mainMenuLabel.text = mainMenuButton;
    mainMenuLabel.fontSize = 30;
    mainMenuLabel.fontColor = [SKColor blackColor];
    mainMenuLabel.position = CGPointMake(CGRectGetMidX(self.frame), -200);
    mainMenuLabel.name = @"mainMenu";
    SKAction *sceneLabels = [SKAction moveToY:(self.frame.size.height/2) duration:15];
    [mainMenuLabel runAction:sceneLabels];

    
    [self addChild:mainMenuLabel];
}

- (void)GameInstructionsLabel {
    NSString *gameInstructionButton;
    gameInstructionButton = @"Game Instructions";
    
    gameInstructionLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    gameInstructionLabel.text = gameInstructionButton;
    gameInstructionLabel.fontSize = 30;
    gameInstructionLabel.fontColor = [SKColor blackColor];
    gameInstructionLabel.position = CGPointMake(CGRectGetMidX(self.frame), -270);
    gameInstructionLabel.name = @"gameInstructions";
    SKAction *sceneLabels = [SKAction moveToY:(self.frame.size.height/2)-30 duration:15];
    [gameInstructionLabel runAction:sceneLabels];

    
    [self addChild:gameInstructionLabel];
}

- (void)BeginGameLabel {
    NSString *mainMenuSceneButton;
    mainMenuSceneButton = @"Begin Game";
    
    beginGameLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    beginGameLabel.text = mainMenuSceneButton;
    beginGameLabel.fontSize = 30;
    beginGameLabel.fontColor = [SKColor blackColor];
    beginGameLabel.position = CGPointMake(CGRectGetMidX(self.frame), -340);
    beginGameLabel.name = @"startGame";
    SKAction *sceneLabels = [SKAction moveToY:(self.frame.size.height/2)-60 duration:15];
    [beginGameLabel runAction:sceneLabels];
    
    [self addChild:beginGameLabel];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if ([node.name isEqualToString:@"startGame"]) {
        
        SKTransition *reveal = [SKTransition fadeWithDuration:3];
        
            //        GameScene *gameScene = [GameScene sceneWithSize:self.size];
        GameScene *gameScene = [GameScene sceneWithSize:self.size];
        gameScene.scaleMode = SKSceneScaleModeAspectFill;
        
        
        [self.view presentScene:gameScene transition:reveal];
    }
    
    if ([node.name isEqualToString:@"mainMenu"]) {
        
        SKTransition *reveal = [SKTransition fadeWithDuration:3];
        
        MainMenu *gameScene = [MainMenu sceneWithSize:self.size];
        gameScene.scaleMode = SKSceneScaleModeAspectFill;
        
        [self.view presentScene:gameScene transition:reveal];
    }
    
    if ([node.name isEqualToString:@"gameInstructions"]) {
        
        SKTransition *reveal = [SKTransition fadeWithDuration:3];
        
        GameInstructions *gameScene = [GameInstructions sceneWithSize:self.size];
        gameScene.scaleMode = SKSceneScaleModeAspectFill;
        
        [self.view presentScene:gameScene transition:reveal];
    }
    
    
}


@end

