//
//  MainMenu.m
//  StarCommander
//
//  Created by Michael on 7/26/15.
//  Copyright (c) 2015 Michael Usry. All rights reserved.
//

#import <CoreMotion/CoreMotion.h>
#import "MainMenu.h"
#import <AVFoundation/AVFoundation.h>
#import "GameScene.h"
#import "GameCredits.h"
#import "GameInstructions.h"
#import "GameKit/Gamekit.h"
#import "GameConsants.h"


@implementation SKScene (Unarchive)

+ (instancetype)unarchiveFromFile:(NSString *)file {
    /* Retrieve scene file path from the application bundle */
    NSString *nodePath = [[NSBundle mainBundle] pathForResource:file ofType:@"sks"];
    /* Unarchive the file to an SKScene object */
    NSData *data = [NSData dataWithContentsOfFile:nodePath
                                          options:NSDataReadingMappedIfSafe
                                            error:nil];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:self forClassName:@"SKScene"];
    SKScene *scene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [arch finishDecoding];
    
    return scene;
}

@end

@implementation MainMenu
{

}

-(instancetype)initWithSize:(CGSize)size{
    
    if (self = [super initWithSize:size]) {
        /* Setup MainMenu scene  */
        

    }
    return self;
    
    
}

- (void)BeginGameLabel {
    NSString *mainMenuSceneButton;
    mainMenuSceneButton = @"Begin Game";
    
    SKLabelNode *startGameLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    startGameLabel.text = mainMenuSceneButton;
    startGameLabel.fontSize = 30;
    startGameLabel.fontColor = [SKColor blackColor];
    startGameLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    startGameLabel.name = @"startGame";
    
    [self addChild:startGameLabel];
}

- (void)GameCreditsLabel {
    NSString *gameCreditsButton;
    gameCreditsButton = @"Game Credits";
    
    SKLabelNode *gameCreditsLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    gameCreditsLabel.text = gameCreditsButton;
    gameCreditsLabel.fontSize = 30;
    gameCreditsLabel.fontColor = [SKColor blackColor];
    gameCreditsLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)+50);
    gameCreditsLabel.name = @"gameCredits";
    
    [self addChild:gameCreditsLabel];
}

- (void)GameCenterLabel {
    NSString *gameCenterButton;
    gameCenterButton = @"Game Center";
    
    SKLabelNode *gameCenterLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    gameCenterLabel.text = gameCenterButton;
    gameCenterLabel.fontSize = 30;
    gameCenterLabel.fontColor = [SKColor blackColor];
    gameCenterLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)+150);
    gameCenterLabel.name = @"gameCenter";
    
    [self addChild:gameCenterLabel];
}

- (void)GameAchievementsLabel {
    NSString *gameAchievementsButton;
    gameAchievementsButton = @"Achievements";
    
    SKLabelNode *GameAchievementsLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    GameAchievementsLabel.text = gameAchievementsButton;
    GameAchievementsLabel.fontSize = 30;
    GameAchievementsLabel.fontColor = [SKColor blackColor];
    GameAchievementsLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)+200);
    GameAchievementsLabel.name = @"gameAchievements";
    
    [self addChild:GameAchievementsLabel];
}


- (void)GameInstructionsLabel {
    NSString *gameInstructionButton;
    gameInstructionButton = @"Game Instructions";
    
    SKLabelNode *gameInstructionLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    gameInstructionLabel.text = gameInstructionButton;
    gameInstructionLabel.fontSize = 30;
    gameInstructionLabel.fontColor = [SKColor blackColor];
    gameInstructionLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)+100);
    gameInstructionLabel.name = @"gameInstructions";
    
    [self addChild:gameInstructionLabel];
}


- (void)didMoveToView:(SKView *)view {
    
        // Dummy Highscore
    NSUInteger highscore = 100;
    
    [self reportScore:highscore forLeaderboardID:kHighScoreLeaderboardCategory];
    
        //    self.backgroundColor = [SKColor redColor];
    [self background];
    
    [self BeginGameLabel];
    
    [self GameInstructionsLabel];
    
    [self GameCreditsLabel];
    
    [self GameCenterLabel];
    
    [self GameAchievementsLabel];
    
    [self startup];

}

-(void)startup{
    NSLog(@"MAIN MENU:pos: x:%f,y:%f",self.frame.size.width,self.frame.size.height);

}

- (void)background {
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"starBackground"];
    background.size = self.frame.size;
    background.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    [self addChild:background];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if ([node.name isEqualToString:@"startGame"]) {
        
        SKTransition *reveal = [SKTransition fadeWithDuration:3];
        
        GameScene *gameScene = [GameScene sceneWithSize:self.size];
        gameScene.scaleMode = SKSceneScaleModeAspectFill;

        
        [self.view presentScene:gameScene transition:reveal];
    }
    
    if ([node.name isEqualToString:@"gameCredits"]) {
        
        SKTransition *reveal = [SKTransition fadeWithDuration:3];
        
        GameCredits *gameScene = [GameCredits sceneWithSize:self.size];
        gameScene.scaleMode = SKSceneScaleModeAspectFill;
        
        [self.view presentScene:gameScene transition:reveal];
    }

    if ([node.name isEqualToString:@"gameInstructions"]) {
        
        SKTransition *reveal = [SKTransition fadeWithDuration:3];
        
        GameInstructions *gameScene = [GameInstructions sceneWithSize:self.size];
        gameScene.scaleMode = SKSceneScaleModeAspectFill;
        
        [self.view presentScene:gameScene transition:reveal];
    }
    
    if ([node.name isEqualToString:@"gameCenter"]) {
        
        
        [self showGameCenter];
    }
    
    if ([node.name isEqualToString:@"gameAchievements"]) {
        
        
        [self showLeaderboard];
    }



}

- (void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */

}

- (void) showGameCenter
{
    GKGameCenterViewController *leaderboardController = [[GKGameCenterViewController alloc] init];
    if (leaderboardController != NULL)
    {
        leaderboardController.leaderboardIdentifier = @"high.score.starcommander";
        leaderboardController.viewState = GKGameCenterViewControllerStateLeaderboards;
//        leaderboardController.gameCenterDelegate = self;
        UIViewController *vc = self.view.window.rootViewController;
        [vc presentViewController: leaderboardController animated: YES completion:nil];
    }
}

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)viewController
{
    UIViewController *vc = self.view.window.rootViewController;
    [vc dismissViewControllerAnimated:YES completion:nil];
}

- (void) showLeaderboard
{
    GKGameCenterViewController *gameCenterController = [[GKGameCenterViewController alloc] init];
    if (gameCenterController != nil)
    {
        gameCenterController.viewState = GKGameCenterViewControllerStateAchievements;
        
        UIViewController *vc = self.view.window.rootViewController;
        [vc presentViewController: gameCenterController animated: YES completion:nil];

    }
}
- (void)leaderboardViewControllerDidFinish:(GKGameCenterViewController *)viewController
{
    UIViewController *vc = self.view.window.rootViewController;
    [vc dismissViewControllerAnimated:YES completion:nil];
}

- (void)reportScore:(int64_t)score forLeaderboardID:(NSString*)identifier
{
    GKScore *scoreReporter = [[GKScore alloc] initWithLeaderboardIdentifier: identifier];
    scoreReporter.value = score;
    scoreReporter.context = 0;
    
    NSLog(@"score: %lli",score);
    NSLog(@"indentifier %@",identifier);
    
    [GKScore reportScores:@[scoreReporter] withCompletionHandler:^(NSError *error) {
        if (error == nil) {
            NSLog(@"Score reported successfully!");
        } else {
            NSLog(@"Unable to report score!");
        }
    }];
}


@end
