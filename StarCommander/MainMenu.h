//
//  MainMenu.h
//  StarCommander
//
//  Created by Michael on 7/26/15.
//  Copyright (c) 2015 Michael Usry. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <GameKit/GameKit.h>

@interface MainMenu : SKScene

@property(assign, nonatomic) id< GKGameCenterControllerDelegate > gameCenterDelegate;
@property(assign, nonatomic) GKGameCenterViewControllerState viewState;
@property(assign, nonatomic) id< GKGameCenterControllerDelegate > leaderboardDelegate;
@property(nonatomic, retain) NSString *leaderboardIdentifier;
@property(assign, nonatomic) id< GKGameCenterControllerDelegate > achievementDelegate;


@end
