    //
    //  GameScene.m
    //  StarCommander
    //
    //  Created by Michael on 7/6/15.
    //  Copyright (c) 2015 Michael Usry. All rights reserved.
    //
#import <CoreMotion/CoreMotion.h>
#import "GameScene.h"
#import <AVFoundation/AVFoundation.h>
#import <GameKit/GameKit.h>
#import "GameConsants.h"

@implementation GameScene
{
    SKSpriteNode *shieldButton;
    SKSpriteNode *fireButton;
    SKSpriteNode *pauseButton;
    SKAction *fireSFX;
    SKAction *hitSFX;
    SKAction *shipDestroyedSFX;
    SKAction *shieldsSFX;
    SKAction *shipHitSFX;
    Boolean shieldsOn, isGameCenterEnabled;
    SKSpriteNode *bossShip, *explodedAsteriod;
    SKSpriteNode *playerShip;
    SKSpriteNode *playerLaser, *leftCanon, *rightCanon;
    SKSpriteNode *enemyLaser, *enemyShip;
    SKSpriteNode *smallAsteroid;
    SKSpriteNode *largeAsteroid;
    SKSpriteNode *shieldsUp, *asteroidHit, *killShields, *multipleCannons, *floatingMines;
    Boolean isPaused,isGameOver,isShieldDamaged, isMultipleCannons, isLevelComplete;
    SKSpriteNode *b1,*b2,*b3,*b4,*b5,*b6,*b7;
    NSArray *asteroidAnimation;
    int playerExtraLives, enemyShipCount, numberOfMines, numberOfShieldPowerUps, numberOfMultipleCannons, measurementlevel, incLevel, negLevel;
    SKLabelNode* scoreLabel;
    SKLabelNode* playerScoreHUD;
    SKLabelNode *gameLabel;
    SKLabelNode *restartLabel;
    double gameOverTime;
    long long playerScore;
    float incrementalProgress;
    
    
    
    
}
typedef enum {
    kEndReasonWin,
    kEndReasonLose
} EndReason;


static const uint32_t shieldsCategory = 0x1;
static const uint32_t laserCategory = 0x1 << 1;
static const uint32_t enemyCategory = 0x1 << 2;
static const uint32_t enemyLaserCategory = 0x1 << 3;
static const uint32_t shipCategory = 0x1 << 4;
static const uint32_t mineCategory = 0x1 << 5;
static const uint32_t killShieldsCategory = 0x1 << 6;



- (void)didBeginContact:(SKPhysicsContact *)contact{
        //What happens on node contact?
    
    NSLog(@"contact detected");
    
    SKPhysicsBody *firstBody;
    SKPhysicsBody *secondBody;
    SKAction *hit = [SKAction sequence:@[shipDestroyedSFX]];
    
    if (!contact.bodyA.node.parent || !contact.bodyB.node.parent) return;
    
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
            //        NSLog(@"contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask");
    }
    else
    {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
            //        NSLog(@"else");
    }
    
        //contact code
    NSLog(@"HIT");
    NSLog(@"firstBody: %@",firstBody);
    NSLog(@"secondBody: %@",secondBody);
    
        //in the player's ship has been hit deduct a life
    if (contact.bodyA.node == playerShip){
        playerExtraLives --;
        NSLog(@"playerExtraLives %i",playerExtraLives);
        if (playerExtraLives <0) {
            NSLog(@"Game is Over");
            isGameOver = YES;
        }else{
            [self startNewGame];
        }
    }
    
        //player hits largeAsteroid
    if (secondBody.node == largeAsteroid) {
        NSLog(@"Score before adding: %lli", playerScore);
        NSLog(@"Ship hit %@", secondBody.node.name);
        
        enemyShipCount --;
        [self adjustScore:100];
        NSLog(@"Score: %lli",playerScore);
        [contact.bodyA.node removeFromParent];
        [contact.bodyB.node removeFromParent];
        
    }
    
        //player hits enemyShip
    if (secondBody.node == enemyShip) {
        NSLog(@"Score before adding: %lli", playerScore);
        NSLog(@"Ship hit %@", secondBody.node.name);

            //Achievement to Game Center
        [self reportAchievement:@"measurement.second.lieutenant" percentComplete:incrementalProgress + 10];

        [self adjustScore:125];
        enemyShipCount --;
        
        NSLog(@"Score: %lli",playerScore);
        [contact.bodyA.node removeFromParent];
        [contact.bodyB.node removeFromParent];
        
    }
    
        //player hits bossShip
    if (secondBody.node == bossShip) {
        NSLog(@"Score before adding: %lli", playerScore);
        NSLog(@"Ship hit %@", secondBody.node.name);
        
            //Achievement to Game Center
        [self reportAchievement:@"inc_lv1" percentComplete:100];

        [self adjustScore:200];
        
        NSLog(@"Score: %lli",playerScore);
        [contact.bodyA.node removeFromParent];
        [contact.bodyB.node removeFromParent];
        
    }
    
        //player smallAsteroid
    if (secondBody.node == smallAsteroid) {
        NSLog(@"Score before adding: %lli", playerScore);
        NSLog(@"Ship hit %@", secondBody.node.name);
        [self adjustScore:50];
        enemyShipCount --;
        
        NSLog(@"Score: %lli",playerScore);
        [contact.bodyA.node removeFromParent];
        [contact.bodyB.node removeFromParent];
        
    }
    
    if (secondBody.node == floatingMines) {
        NSLog(@"Score before adding: %lli", playerScore);
        NSLog(@"Ship hit %@", secondBody.node.name);
        enemyShipCount --;
        [self adjustScore:25];
        
        NSLog(@"Score: %lli",playerScore);
        [contact.bodyA.node removeFromParent];
        [contact.bodyB.node removeFromParent];
        
    }
    
    if (secondBody.node == killShields) {
        NSLog(@"shields are damaged.");
        isShieldDamaged = YES;
        NSLog(@"Shield status: %i",isShieldDamaged);
        
        enemyShipCount --;
        [self adjustScore:25];
        [contact.bodyA.node removeFromParent];
        [contact.bodyB.node removeFromParent];
        
    }
    
    if (secondBody.node == multipleCannons){
        NSLog(@"add multiple Canons.");
        isMultipleCannons = YES;
        enemyShipCount --;
        [self adjustScore:25];
        [contact.bodyA.node removeFromParent];
        [contact.bodyB.node removeFromParent];
        
    }
    
    
    [contact.bodyA.node removeFromParent];
    [contact.bodyB.node removeFromParent];
    
    [self runAction:hit];
    
}

- (void)didMoveToView:(SKView *)view {
    
        //preload audio SFX
    fireSFX = [SKAction playSoundFileNamed:@"LasersShot.mp3" waitForCompletion:NO];
    shieldsSFX = [SKAction playSoundFileNamed:@"shields.mp3" waitForCompletion:NO];
    shipDestroyedSFX = [SKAction playSoundFileNamed:@"shipDestroyed.mp3" waitForCompletion:NO];
    shipHitSFX = [SKAction playSoundFileNamed:@"shieldHit.mp3" waitForCompletion:NO];
    
        //set Bools
    isPaused = NO;
    isGameOver = NO;
    
        //initial player info - set to 1 for testing
    playerExtraLives = 0;
    playerScore = 0;
    
        //inital enemyShipCount
    enemyShipCount = 3;
    
        //set gravity and delegate
    self.physicsWorld.gravity = CGVectorMake(0, -.1);
    self.physicsWorld.contactDelegate = self;
    
    [self setupHud];
    [self startNewGame];
    
        //need to setup motion for playerShip
    
}

- (void)startNewGame {
    
        //set objects on scene
    isShieldDamaged = NO;
    double curTime = CACurrentMediaTime();
    gameOverTime = curTime + 30.0;
    
    NSLog(@"gameOverTime: %f",gameOverTime);
    
    [gameLabel removeFromParent];
    [restartLabel removeFromParent];
    [self background];
    [self bossShip];
    [self playerShip];
    [self largeAsteroid];
    [self smallAsteroid];
    [self shieldButton];
    [self fireButton];
    [self enemyShip];
    [self pauseButton];
    [self playerLaser];
    
    enemyShipCount = 3;
    
    NSLog(@"GAME SCENE pos: x:%f,y:%f",self.frame.size.width,self.frame.size.height);
    
    numberOfMines = [self getRandomNumberBetween:2 to:10];
    numberOfMultipleCannons = [self getRandomNumberBetween:2 to:4];
    numberOfShieldPowerUps = [self getRandomNumberBetween:2 to:6];
    for (int nom; nom <= numberOfMines; nom++) {
        [self floatingMines];
    }
    for (int nomc; nomc <= numberOfMultipleCannons; nomc++) {
        [self multipleCannons];
    }
    for (int nosp; nosp <= numberOfShieldPowerUps; nosp++) {
        [self killShields];
    }
    enemyShipCount = enemyShipCount + numberOfShieldPowerUps + numberOfMultipleCannons + numberOfMines;
    
    
}

- (void)background {
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"starBackground"];
    background.size = self.frame.size;
    background.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    [self addChild:background];
}

- (void)playerShip {
    playerShip = [SKSpriteNode spriteNodeWithImageNamed:@"player"];
    playerShip.xScale = .75;
    playerShip.yScale = .75;
    playerLaser.name = @"playerShip";
    playerShip.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)-200);
    playerShip.name = @"playerShipNode";
    
    playerShip.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:(playerShip.size.width/2)];
    playerShip.physicsBody.usesPreciseCollisionDetection = YES;
    playerShip.physicsBody.dynamic = NO;
    playerShip.physicsBody.categoryBitMask = shipCategory;
    playerShip.physicsBody.contactTestBitMask = enemyCategory | enemyLaserCategory;
    playerShip.physicsBody.collisionBitMask = enemyCategory | enemyLaserCategory;
    
    [self addChild:playerShip];
}

- (void)largeAsteroid {
    largeAsteroid = [SKSpriteNode spriteNodeWithImageNamed:@"asteroidLarge"];
    largeAsteroid.name = @"largeAsteriod";
    largeAsteroid.xScale = .75;
    largeAsteroid.yScale = .75;
    largeAsteroid.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)+433);
    largeAsteroid.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:(largeAsteroid.size.width/2)];
    largeAsteroid.physicsBody.usesPreciseCollisionDetection = YES;
    largeAsteroid.physicsBody.dynamic = YES;
    largeAsteroid.physicsBody.affectedByGravity = YES;
    largeAsteroid.physicsBody.mass = .1;
    largeAsteroid.speed = .1;
    
    largeAsteroid.physicsBody.categoryBitMask = enemyCategory;
    largeAsteroid.physicsBody.contactTestBitMask = laserCategory | shipCategory | shieldsCategory;
    largeAsteroid.physicsBody.collisionBitMask = laserCategory | shipCategory;
    
    [self addChild:largeAsteroid];
}

- (void)leftCanon {
    leftCanon = [SKSpriteNode spriteNodeWithImageNamed:@"laserGreen"];
    leftCanon.xScale = .5;
    leftCanon.yScale = .5;
    leftCanon.name = @"leftCanon";
    leftCanon.physicsBody = [SKPhysicsBody bodyWithTexture:leftCanon.texture size:leftCanon.texture.size];
    leftCanon.physicsBody.usesPreciseCollisionDetection = YES;
    leftCanon.physicsBody.categoryBitMask = laserCategory;
    leftCanon.physicsBody.affectedByGravity = NO;
    
    [self addChild:leftCanon];
}

- (void)rightCanon {
    rightCanon = [SKSpriteNode spriteNodeWithImageNamed:@"laserGreen"];
    rightCanon.xScale = .5;
    rightCanon.yScale = .5;
    rightCanon.name = @"rightCanon";
    rightCanon.physicsBody = [SKPhysicsBody bodyWithTexture:rightCanon.texture size:rightCanon.texture.size];
    rightCanon.physicsBody.usesPreciseCollisionDetection = YES;
    rightCanon.physicsBody.categoryBitMask = laserCategory;
    rightCanon.physicsBody.affectedByGravity = NO;
    
    [self addChild:rightCanon];
}

- (void)playerLaser {
    playerLaser = [SKSpriteNode spriteNodeWithImageNamed:@"laserGreen"];
    playerLaser.xScale = .75;
    playerLaser.yScale = .75;
    playerLaser.name = @"playerLaser";
    playerLaser.physicsBody = [SKPhysicsBody bodyWithTexture:playerLaser.texture size:playerLaser.texture.size];
    playerLaser.physicsBody.usesPreciseCollisionDetection = YES;
    playerLaser.physicsBody.categoryBitMask = laserCategory;
    playerLaser.physicsBody.affectedByGravity = NO;
    
    [self addChild:playerLaser];
}

- (void)killShields {
    
    killShields = [SKSpriteNode spriteNodeWithImageNamed:@"damagedShields"];
    killShields.name = @"killShields";
    int xPosition = [self getRandomNumberBetween:200 to:700];
    int yPosition = [self getRandomNumberBetween:1000 to:1600];
    NSLog(@"killShields xPos: %i",xPosition);
    NSLog(@"killShields yPos: %i",yPosition);
    
    killShields.position = CGPointMake(xPosition, yPosition);
    killShields.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:(killShields.size.width/2)];
    killShields.physicsBody.friction = .001;
    killShields.physicsBody.usesPreciseCollisionDetection = YES;
    killShields.physicsBody.categoryBitMask = enemyCategory;
    killShields.physicsBody.contactTestBitMask = laserCategory | shipCategory | killShieldsCategory;
    killShields.physicsBody.collisionBitMask = laserCategory | shipCategory | killShieldsCategory;
    
    [self addChild:killShields];
}

- (void)floatingMines {
    
    floatingMines = [SKSpriteNode spriteNodeWithImageNamed:@"mines"];
    floatingMines.name = @"floatingMines";
        //getx and y positions
        //x -300 : 400;
        //y 700 : 1100:
    int xPosition = [self getRandomNumberBetween:200 to:700];
    int yPosition = [self getRandomNumberBetween:700 to:1000];
    NSLog(@"floatingMines xPos: %i",xPosition);
    NSLog(@"floatingMines yPos: %i",yPosition);
    floatingMines.xScale = .5;
    floatingMines.yScale = .5;
    
    
    floatingMines.position = CGPointMake(xPosition,yPosition);
    floatingMines.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:(floatingMines.size.width/2)];
    floatingMines.physicsBody.friction = .001;
    floatingMines.physicsBody.usesPreciseCollisionDetection = YES;
    floatingMines.physicsBody.categoryBitMask = enemyCategory;
    floatingMines.physicsBody.contactTestBitMask = laserCategory | shipCategory | mineCategory;
    floatingMines.physicsBody.collisionBitMask = laserCategory | shipCategory | mineCategory;
    
    [self addChild:floatingMines];
}

- (void)multipleCannons {
    
    multipleCannons = [SKSpriteNode spriteNodeWithImageNamed:@"multipleCanons"];
    multipleCannons.name = @"multipleCannons";
        //getx and y positions
        //x -300 : 400;
        //y 700 : 1100:
    int xPosition = [self getRandomNumberBetween:200 to:700];
    int yPosition = [self getRandomNumberBetween:1200 to:1500];
    NSLog(@"xPos: %i",xPosition);
    NSLog(@"yPos: %i",yPosition);
    
    multipleCannons.position = CGPointMake(xPosition, yPosition);
    
    multipleCannons.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:(multipleCannons.size.width/2)];
    multipleCannons.physicsBody.friction = .001;
    multipleCannons.physicsBody.usesPreciseCollisionDetection = YES;
    multipleCannons.physicsBody.categoryBitMask = enemyCategory;
    
    [self addChild:multipleCannons];
}



- (void)shieldsUp {
    
    shieldsUp = [SKSpriteNode spriteNodeWithImageNamed:@"shield"];
    shieldsUp.name = @"shieldsUp";
    shieldsUp.physicsBody = [SKPhysicsBody bodyWithTexture:shieldsUp.texture size:shieldsUp.texture.size];
    shieldsUp.physicsBody.usesPreciseCollisionDetection = YES;
    shieldsUp.physicsBody.categoryBitMask = shieldsCategory;
    shieldsUp.physicsBody.contactTestBitMask = enemyCategory | enemyLaserCategory;
    shieldsUp.physicsBody.collisionBitMask = enemyCategory | enemyLaserCategory;
    shieldsUp.physicsBody.affectedByGravity = NO;
    shieldsUp.physicsBody.dynamic = NO;
    
    
    [self addChild:shieldsUp];
}

- (void)enemyLaser {
    enemyLaser = [SKSpriteNode spriteNodeWithImageNamed:@"laserRed"];
    enemyLaser.xScale = .75;
    enemyLaser.yScale = .75;
    enemyLaser.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)+200);
    enemyLaser.name = @"enemyLaser";
    enemyLaser.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:(enemyLaser.size.width/2)];
    enemyLaser.physicsBody.friction = .001;
    enemyLaser.physicsBody.usesPreciseCollisionDetection = YES;
    enemyLaser.physicsBody.categoryBitMask = laserCategory;
    enemyLaser.physicsBody.affectedByGravity = NO;
    
    
    [self addChild:enemyLaser];
}

- (void)enemyShip {
    enemyShip = [SKSpriteNode spriteNodeWithImageNamed:@"enemyShip"];
    enemyShip.xScale = .75;
    enemyShip.yScale = .75;
    enemyShip.name = @"enemyShip";
    enemyShip.position = CGPointMake(CGRectGetMidX(self.frame)-100, CGRectGetMidY(self.frame)+470);
    enemyShip.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:(enemyShip.size.width/2)];
    enemyShip.physicsBody.friction = .001;
    enemyShip.physicsBody.usesPreciseCollisionDetection = YES;
    enemyShip.physicsBody.categoryBitMask = enemyCategory;
    enemyShip.physicsBody.contactTestBitMask = laserCategory | shipCategory | shieldsCategory;
    enemyShip.physicsBody.collisionBitMask = laserCategory | shipCategory | shieldsCategory;
    
    [self addChild:enemyShip];
}

- (void)bossShip {
    bossShip = [SKSpriteNode spriteNodeWithImageNamed:@"bossShip"];
    bossShip.xScale = .75;
    bossShip.yScale = .75;
    bossShip.name = @"bossShip";
    bossShip.position = CGPointMake(CGRectGetMidX(self.frame)-200, CGRectGetMidY(self.frame)+360);
    bossShip.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:(bossShip.size.width/2)];
    bossShip.physicsBody.friction = .001;
    bossShip.physicsBody.usesPreciseCollisionDetection = YES;
    bossShip.physicsBody.categoryBitMask = enemyCategory;
    bossShip.physicsBody.contactTestBitMask = laserCategory | shipCategory | shieldsCategory;
    bossShip.physicsBody.collisionBitMask = laserCategory | shipCategory;
    bossShip.physicsBody.affectedByGravity = NO;
    
        //animation (rotation) of The Boss
    SKAction *oneRevolution = [SKAction rotateByAngle:-M_PI*2 duration:6.0];
    SKAction *bossMoveAction = [SKAction moveToX:1000 duration:5];
    SKAction *sequence = [SKAction group:@[bossMoveAction,oneRevolution]];
    
    
    [bossShip runAction:sequence];
    
    
    
    [self addChild:bossShip];
}

- (void)smallAsteroid {
    smallAsteroid = [SKSpriteNode spriteNodeWithImageNamed:@"asteroidSmall"];
    smallAsteroid.xScale = .75;
    smallAsteroid.yScale = .75;
    smallAsteroid.name = @"smallAsteriod";
    smallAsteroid.position = CGPointMake(CGRectGetMidX(self.frame)+122, CGRectGetMidY(self.frame)+400);
    smallAsteroid.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:(smallAsteroid.size.width/2)];
    smallAsteroid.physicsBody.usesPreciseCollisionDetection = YES;
    smallAsteroid.physicsBody.dynamic = YES;
    smallAsteroid.physicsBody.categoryBitMask = enemyCategory;
    smallAsteroid.physicsBody.contactTestBitMask = laserCategory | shipCategory | shieldsCategory;
    smallAsteroid.physicsBody.collisionBitMask = laserCategory | shipCategory;
    
    
    
    [self addChild:smallAsteroid];
}

- (void)explodedAsteriod {
    
    explodedAsteriod = [SKSpriteNode spriteNodeWithImageNamed:@"asteroidSmall"];
    explodedAsteriod.xScale = .75;
    explodedAsteriod.yScale = .75;
    explodedAsteriod.name = @"smallAsteriod";
        //explodedAsteriod.position = CGPointMake(CGRectGetMidX(self.frame)+122, CGRectGetMidY(self.frame)+250);
    explodedAsteriod.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:(smallAsteroid.size.width/2)];
    explodedAsteriod.physicsBody.usesPreciseCollisionDetection = YES;
    explodedAsteriod.physicsBody.categoryBitMask = enemyCategory;
    
    
    [self addChild:explodedAsteriod];
}

- (void)shieldButton {
    shieldButton = [SKSpriteNode spriteNodeWithImageNamed:@"shieldButton"];
        //    shieldButton.position = CGPointMake(CGRectGetMidX(self.frame)-160,CGRectGetMinY(self.frame)+60);
    shieldButton.position = CGPointMake(CGRectGetMidX(self.frame)-160,CGRectGetMinY(self.frame)+60);
    [self addChild:shieldButton];
}

- (void)pauseButton {
    pauseButton = [SKSpriteNode spriteNodeWithImageNamed:@"pauseButton"];
        //    pauseButton.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)-325);
    pauseButton.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMinY(self.frame)+60);
    
    [self addChild:pauseButton];
}

- (void)fireButton {
    NSLog(@"GAME SCENE pos: x:%f,y:%f",self.frame.size.width,self.frame.size.height);
    NSLog(@"posmidminx: x:%f,y:%f",CGRectGetMinX(self.frame),self.frame.size.height);
        //    NSLog(@"self.frame.size.width %f",self.frame.size.width);
    
    fireButton = [SKSpriteNode spriteNodeWithImageNamed:@"fireButton"];
    fireButton.position = CGPointMake(CGRectGetMidX(self.frame)+160, CGRectGetMinY(self.frame)+63);
    
    [self addChild:fireButton];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    
    CGPoint touchLocation = [[touches anyObject] locationInNode:self];
    float currentPlayerPositionX = playerShip.position.x;
    float currentPlayerPositionY = playerShip.position.y;
    
    NSLog(@"ship x position %f", currentPlayerPositionX);
    NSLog(@"ship y position %f", currentPlayerPositionY);
    
    if ([restartLabel containsPoint:touchLocation]) {
        NSLog(@"restartLabel pressed");
        [[self childNodeWithName:@"restartLabel"] removeFromParent];
        [[self childNodeWithName:@"winLoseLabel"] removeFromParent];
        [gameLabel removeAllChildren];
        [restartLabel removeAllChildren];
        [self startNewGame];
        SKAction *fadeAction = [SKAction fadeOutWithDuration:1.0];
        [gameLabel runAction:fadeAction completion:^{[gameLabel removeFromParent];}];
        [restartLabel runAction:fadeAction completion:^{[gameLabel removeFromParent];}];
        
        
        
    }
    
    if([shieldButton containsPoint:touchLocation]){
        if (!isShieldDamaged) {
            
            if (!shieldsOn) {
                shieldButton.alpha = 0.5;
                NSLog(@"shieldButton.shieldsOn");
                
                    //SET SHIELDS
                    //show shields on screen
                
                shieldsUp.position = CGPointMake(currentPlayerPositionX, currentPlayerPositionY+20);
                
                [self shieldsUp];
                
                SKAction *raiseShields = [SKAction sequence:@[shieldsSFX]];
                [self runAction:raiseShields];
            }
            
        }
    }
    
    if([fireButton containsPoint:touchLocation]){
        fireButton.alpha = 0.5;
        
        NSLog(@"fireButton pressed");
            //set the laser just above the player ship
        
        SKAction *laserMoveAction = [SKAction moveToY:currentPlayerPositionY+1000 duration:2];
        
        SKAction *fire = [SKAction sequence:@[fireSFX,laserMoveAction]];
        
        playerLaser.position = CGPointMake(currentPlayerPositionX, currentPlayerPositionY+55);
        
        [playerLaser runAction:fire];
        [self playerLaser];
        
        if (isMultipleCannons) {
            NSLog(@"firebutton isMultipleCannons");
            leftCanon.position = CGPointMake(currentPlayerPositionX - 60, currentPlayerPositionY+55);
            rightCanon.position = CGPointMake(currentPlayerPositionX + 60, currentPlayerPositionY+55);
            
            [rightCanon runAction:fire];
            [leftCanon runAction:fire];
            [self leftCanon];
            [self rightCanon];
            
            
        }
        
    }
    
    if([pauseButton containsPoint:touchLocation]){
        
        if (isPaused) {
            NSLog(@"isPaused"); // GAME IS PAUSED
            pauseButton.alpha = 1.0;
            
            self.scene.view.paused = NO;
            
            isPaused = NO;
            
        }else{
            NSLog(@"!isPaused"); // GAME IS NOT PAUSED
            
            pauseButton.alpha = .5;
            self.scene.view.paused = YES;
            
            isPaused = YES;
        }
        
    }
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if(shieldButton.alpha == 0.5){
        shieldButton.alpha = 1.0;
        
        shieldsOn = NO;
    }
    
    if(fireButton.alpha == 0.5){
        fireButton.alpha = 1.0;
        [self fireButton];
        
    }
    
}

- (void)setupHud {
    scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Courier"];
    scoreLabel.name = @"scoreHudName";
    scoreLabel.fontSize = 18;
    scoreLabel.fontColor = [SKColor whiteColor];
    scoreLabel.text = [NSString stringWithFormat:@"Score: %06u", 0];
    scoreLabel.position = CGPointMake((self.view.frame.size.width)+20,(self.view.frame.size.height)+60);
    [self addChild:scoreLabel];
    
}

- (void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    NSLog(@"Shields status %i",isShieldDamaged);
    
        //current computer time
    double curTime = CACurrentMediaTime();
    NSLog(@"curTime: %f",curTime);
    NSLog(@"gameOverTime: %f",gameOverTime);
    
        // Add at end of update loop
    if (playerExtraLives < 0) {
        NSLog(@"you lose...");
        [self endTheScene:kEndReasonLose];
        
    }
    
    NSLog(@"enemyShipCount: %i",enemyShipCount);
    
    if (curTime >= gameOverTime){
        NSLog(@"curTime >= gameOverTime");
        isGameOver = YES;
        [self endTheScene:kEndReasonWin];
        
    }
    
    NSLog(@"playerLaser.position.y %f",playerLaser.position.y);
    NSLog(@"playerLaser.position.x %f",playerLaser.position.x);
    
    
    if (playerLaser.position.y > 200) {
        [playerLaser removeFromParent];
    }
    
    if (leftCanon.position.y > 768) {
        [leftCanon removeFromParent];
    }
    
    if (rightCanon.position.y > 768) {
        [rightCanon removeFromParent];
    }
    
    if (isShieldDamaged) {
        NSLog(@"Shields Are Damaged");
        [shieldButton removeFromParent];
    }else{
        [self shieldButton];
    }
    
    
}

- (void)adjustScore:(int)points {
    playerScore += points;
    [scoreLabel removeFromParent];
        //    scoreLabel.text = [NSString stringWithFormat:@"Score: %04u", playerScore];
    scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Courier"];
    scoreLabel.name = @"scoreHudName";
    scoreLabel.fontSize = 18;
    scoreLabel.fontColor = [SKColor whiteColor];
    scoreLabel.text = [NSString stringWithFormat:@"Score: %06lld", playerScore];
    scoreLabel.position = CGPointMake((self.view.frame.size.width)+20,(self.view.frame.size.height)+60);
    [self addChild:scoreLabel];
    
}

- (void)endTheScene:(EndReason)endReason {
    if (!isGameOver) {
        NSLog(@"!isGameOver");
        return;
    }
    
    [self removeAllActions];
    isGameOver = YES;
    
    NSString *message;
    if (endReason == kEndReasonWin) {
        message = @"Scene Over!";
        
            //Achievement to Game Center
        [self reportAchievement:@"complete_lvl1" percentComplete:100];

    } else if (endReason == kEndReasonLose) {
        message = @"Game Over!";

            //Achievement to Game Center
        [self reportAchievement:@"negative_1" percentComplete:100];
    }
    
    gameLabel = [[SKLabelNode alloc] initWithFontNamed:@"Courier Bold"];
    gameLabel.name = @"winLoseLabel";
    gameLabel.text = message;
    gameLabel.scale = 1;
    gameLabel.position = CGPointMake(self.frame.size.width/2, self.frame.size.height * 0.6);
    gameLabel.fontColor = [SKColor whiteColor];
    [self addChild:gameLabel];
    
    restartLabel = [[SKLabelNode alloc] initWithFontNamed:@"Courier Bold"];
    restartLabel.name = @"restartLabel";
    restartLabel.text = @"Play Again?";
    restartLabel.scale = 1;
    restartLabel.position = CGPointMake(self.frame.size.width/2, self.frame.size.height * 0.4);
    restartLabel.fontColor = [SKColor whiteColor];
    [self addChild:restartLabel];
    enemyShipCount = 0;
    
        //report score to gamecenter
    NSLog(@"end of game score: %lli", playerScore);
    [self reportScore:playerScore forLeaderboardID:kHighScoreLeaderboardCategory];
    
}

- (int)getRandomNumberBetween:(int)from to:(int)to {
    
    return (int)from + arc4random() % (to-from+1);
}

//- (void)reportScore:(int64_t)score forLeaderboardID:(NSString*)identifier

- (void)reportScore:(int64_t)score forLeaderboardID:(NSString*)identifier
{
    GKScore *scoreReporter = [[GKScore alloc] initWithLeaderboardIdentifier: identifier];
    scoreReporter.value = score;
    scoreReporter.context = 0;
    
    
    NSLog(@"score: %lli",score);
    NSLog(@"indentifier %@",identifier);

    
    [GKScore reportScores:@[scoreReporter] withCompletionHandler:^(NSError *error) {
        if (error == nil) {
            NSLog(@"Score reported successfully!");
        } else {
            NSLog(@"Unable to report score!");
        }
    }];
}

    // method to report achievements to game center
- (void) reportAchievement: (NSString*) identifier percentComplete: (float) percent
{
    GKAchievement *achievement = [[GKAchievement alloc] initWithIdentifier: identifier];
    if (achievement){
        achievement.percentComplete = percent;
        
        [achievement reportAchievementWithCompletionHandler:^(NSError *error) {
            if (error != nil) {
                NSLog(@"Error in reporting achievements: %@", error);
            }
        }];
        achievement.showsCompletionBanner = YES;
    }
}

@end
