//
//  GameScene.h
//  StarCommander
//

//  Copyright (c) 2015 Michael Usry. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameScene : SKScene <SKPhysicsContactDelegate>

@end
