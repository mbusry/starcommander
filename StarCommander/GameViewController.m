//
//  GameViewController.m
//  StarCommander
//
//  Created by Michael on 7/6/15.
//  Copyright (c) 2015 Michael Usry. All rights reserved.
//

#import "GameViewController.h"
#import "GameScene.h"
#import "MainMenu.h"
#import "AppDelegate.h"

@implementation SKScene (Unarchive)

+ (instancetype)unarchiveFromFile:(NSString *)file {
    /* Retrieve scene file path from the application bundle */
    NSString *nodePath = [[NSBundle mainBundle] pathForResource:file ofType:@"sks"];
    /* Unarchive the file to an SKScene object */
    NSData *data = [NSData dataWithContentsOfFile:nodePath
                                          options:NSDataReadingMappedIfSafe
                                            error:nil];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:self forClassName:@"SKScene"];
    SKScene *scene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [arch finishDecoding];
    
    return scene;
}

@end


@implementation GameViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"GameViewController viewDidAppear");
    [self authenticateLocalPlayer];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
        NSLog(@"GameViewController viewDidLoad");

    // Configure the view.
    SKView * skView = (SKView *)self.view;
 // // Sprite Kit applies additional optimizations to improve rendering performance
    skView.ignoresSiblingOrder = YES;
    
    
    MainMenu *mainMenuScene = [MainMenu unarchiveFromFile:@"MainMenu"];
//    mainMenuScene.size = skView.bounds.size;
    mainMenuScene.scaleMode = SKSceneScaleModeAspectFill;
    
        //present scene
    [skView presentScene:mainMenuScene];

    
}


- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (IBAction)pauseButtonPressed:(id)sender {
    NSLog(@"pauseButtonPressed");
    GameScene *scene = [GameScene unarchiveFromFile:@"GameScene"];

    scene.view.paused = YES;

}

- (void) authenticateLocalPlayer{
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
    [localPlayer authenticateWithCompletionHandler:^(NSError *error){
        if (error == nil) {
            NSLog(@"Successfully authenticated localPlayer");
        }else{
            NSLog(@"Failed to authenticate localPlayer. Error: %@",error);
        }
    }];
}


@end
